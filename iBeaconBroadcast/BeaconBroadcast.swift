/*******************************************************************************************************************
|   File: BeaconBroadcast.swift
|   Proyect: IBNdemo
|
|   Description: -
|
|
|   Created by: Daniel Ormeño
|   Created on: 25/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*******************************************************************************************************************/

import Foundation
import CoreLocation
import CoreBluetooth

class BeaconBroadcast: NSObject, CBPeripheralManagerDelegate{
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      =============================//
    
    //- UUIDS for Region
    let regionUUID :NSUUID
    let major: CLBeaconMajorValue
    let minor: CLBeaconMinorValue
    
    // Objects used in the creation of iBeacons
    var beaconRegion = CLBeaconRegion()
    var regionData = NSDictionary()
    var manager = CBPeripheralManager()
    
    var scannedBeacons = [CLBeacon]()
    
    // =====================================     CLASS CONSTRUCTORS      ==========================================//
    init(regionUUID:NSUUID){
        
        //-- UUID for region from parameter
        self.regionUUID = regionUUID
        self.major = 0
        self.minor = 0
        
        //-- BLE beacon region setup
        self.beaconRegion = CLBeaconRegion (proximityUUID: self.regionUUID, major: self.major, minor: self.minor, identifier: "emulatedBeacon")
        
        super.init()
        
        self.regionData = self.beaconRegion.peripheralDataWithMeasuredPower(nil)
        self.manager = CBPeripheralManager (delegate: self, queue: nil, options: nil)
        
    } //END-OF: class Constructor
    
    init(regionUUID:NSUUID, major:CLBeaconMajorValue, minor: CLBeaconMinorValue){
        
        //-- UUID for region from parameter
        self.regionUUID = regionUUID
        self.major = major
        self.minor = minor
        
        //-- BLE beacon region setup
        self.beaconRegion = CLBeaconRegion (proximityUUID: self.regionUUID, major: self.major, minor: self.minor, identifier: "emulatedBeacon")
        
        super.init()
        
        self.regionData = self.beaconRegion.peripheralDataWithMeasuredPower(nil)
        self.manager = CBPeripheralManager (delegate: self, queue: nil, options: nil)
        
    } //END-OF: class Constructor
    
    // =====================================     CLASS METHODS   ======================================================//
    /********************************************************************************************************************
    METHOD NAME: getRegionIdentifierValue
    
    OBSERVATIONS: TODO - Set region identifier when more uuids are added
    
    ********************************************************************************************************************/
    
    func getRegionIdentifierValue() -> String {
        return ""
    }
    
    /********************************************************************************************************************
    METHOD NAME: broadcastRegion
    
    OBSERVATIONS: Broadcasts BLE advertisment signal based on the defined Region, returns description of advertised region
    
    ********************************************************************************************************************/
    
    func broadcastRegion() -> String {
        manager.startAdvertising(self.regionData)
        return regionUUID.UUIDString
    }
    
    /********************************************************************************************************************
    METHOD NAME: stopBroadcastRegion
    
    OBSERVATIONS: Stops the broadcast of the region defined in the instantiation of the object
    
    ********************************************************************************************************************/
    
    func stopBroadcastRegion() {
        manager.stopAdvertising()
    }
    
    // =====================================     PeripheralManagerDelegate      =======================================//
    /********************************************************************************************************************
    METHOD NAME: peripheralManagerDidUpdateState
    
    OBSERVATIONS: Manager delegate method required by the PeripheralManagerDelegate class
    ********************************************************************************************************************/
    func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager!) {
        
        if(peripheral.state == CBPeripheralManagerState.PoweredOn) {
            println("powered on")
        } else if(peripheral.state == CBPeripheralManagerState.PoweredOff) {
            println("powered off")
            manager.stopAdvertising()
        }
    }
    
}