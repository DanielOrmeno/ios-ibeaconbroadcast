/************************************************************************************************************************
|   File: ViewController.swift
|   Proyect: iBeaconBroadcast
|   For: IBNdemo
|
|   Description: -
|
|
|   Created by: Daniel Ormeño
|   Created on: 25/10/2014
|
|   Copyright (c) 2014 Daniel Ormeño. All rights reserved.
*************************************************************************************************************************/

import UIKit
import CoreBluetooth
import CoreLocation

class ViewController: UIViewController {
    
    // =====================================     INSTANCE VARIABLES / PROPERTIES      ==================================//
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var broadcastButton: UIButton!
    
    //-- the UUID our iBeacons will use
    let uuidRegion = NSUUID(UUIDString: "0CF052C2-97CA-407C-84F8-B62AAC4E9020")
    
    //-- BeaconBroadcast array
    var beacons = [BeaconBroadcast]()
    var broadcasting = false
    
    
    // =====================================     VIEW CONTROLLER REQUIRED METHODS      ================================//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //- Sets appropriate labels
        statusLabel.text = "Beacon disabled"
        uuidLabel.text = ""
        
        //- Beacon region config
        beacons.append(BeaconBroadcast(regionUUID: uuidRegion!))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // =====================================     UI INVOKED METHODS      ==============================================//
    
    @IBAction func toggleBroadcast(sender: AnyObject) {
        
        if (!broadcasting) {
            
            //- Starts Broadcast of region
            var labelText = self.beacons.last?.broadcastRegion()
            
            //- Sets appropriate labels
            statusLabel.text = "Broadcasting!"
            uuidLabel.text = labelText
            
            broadcasting = true
            
        } else {
            
            //- Stops broadcast of region
            self.beacons.last?.stopBroadcastRegion()
            
            //- Sets appropriate labels
            statusLabel.text = "Beacon disabled"
            uuidLabel.text = ""
            
            broadcasting = false
        }
        
    }
}

